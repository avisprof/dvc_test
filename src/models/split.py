import click
import pandas as pd
from typing import List

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument('output_files', type=click.Path(), nargs=2)
def split_data(input_file: str, output_files: List[str]):
    df = pd.read_csv(input_file)
    
    train = df.sample(frac=0.80, random_state=200)
    test = df.drop(train.index)

    train.to_csv(output_files[0], index=False)
    test.to_csv(output_files[1], index=False)

if __name__ == '__main__':
    split_data()